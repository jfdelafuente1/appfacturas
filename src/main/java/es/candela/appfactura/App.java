package es.candela.appfactura;

import java.io.*;
import java.util.*;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Aplicación principal de Facturas.
 * @version 1.2, 23/02/21
 * @author JFdelaFuente
 */

public final class App {

    public static final int SALIR = 0;
    public static final int LISTA_CLIENTES = 1;
    public static final int ADD_CLIENTE = 2;
    public static final int ADD_COMPRAS = 3;
    public static final int FACTURAS = 4;
    public static final int BORRAR_CLIENTE = 5;
    public static final int CARGAR_CLIENTES_FICH = 6;
    public static final int SALVAR_CLIENTES_FICH = 7;
    public static final int CARGAR_COMPRAS_FICH = 8;
    public static final int SALVAR_COMPRAS_FICH = 9;
    private static final String NO_EXISTE_CLIENTE = "No existen clientes. Primero debe añadir Clientes a la aplicación.";
    private static final String CLIENTE_NO_ENCONTRADO = "El cliente No encontrado.";
    private static final String INTRO_NOMBRE = ">>>> Introduzca el nombre del cliente : ";

    private ArrayList<Cliente> clientes;
    private static final Logger logger = LogManager.getLogger(App.class);
    private static String DATA_DIR;
    private static String FILE_CLIENTES;
    private static String FILE_COMPRAS;

    App() {
        clientes = new ArrayList<Cliente>();
        System.setProperty("log4j.configurationFile", "log4j2.xml");
        String log4j = System.getProperty("log4j.configurationFile");
        logger.debug("Configuration File Defined To Be :: " + log4j);
        Properties properties= new Properties();
        try (FileInputStream fis = new FileInputStream(new File("configuration.properties"))) {
            properties.load(fis);
            DATA_DIR = properties.getProperty("DATA_DIR");
            FILE_CLIENTES = properties.getProperty("FILE_CLIENTES");
            FILE_COMPRAS = properties.getProperty("FILE_COMPRAS");
            logger.debug("Fichero de properties cargado.");
        } catch (Exception e) {
            logger.warn(e.getMessage());
            logger.warn("Se cargan las properties por defecto.");
            DATA_DIR = "data";
            FILE_CLIENTES = "clientes.txt";
            FILE_COMPRAS = "compras.txt";
        }
    }

    public static void main(String[] args) {
        App app = new App();
        logger.debug("App is running");
        app.menu();
    }

    public void menu() {
        int opcion = -1;
        boolean salir = false;
        while (!salir) {
            opcion = mostrarMenu();
            switch (opcion) {
                case SALIR:
                    salir = true;
                    logger.info("Salimos de la aplicación de Gestión de Facturas. Gracias.\n");
                    logger.debug("La aplicacion ha sido cerrada correctamente por el usuario.");
                    break;
                case LISTA_CLIENTES:
                    System.out.println("\nLista Clientes   ------------");
                    if (! clientes.isEmpty() ) {
                        mostrarListaClientes(clientes);
                    } else {
                        logger.info(NO_EXISTE_CLIENTE);
                    }
                    break;
                case ADD_CLIENTE:
                    System.out.println("\nAñadir Cliente   ------------");
                    Cliente cliente = addCliente();
                    clientes.add(cliente);
                    break;
                case ADD_COMPRAS:
                    System.out.println("\nAñadir Compras de un Clientes   ------------");
                    if (! clientes.isEmpty() ) {
                        addComprasCliente(clientes);
                    } else {
                        logger.info(NO_EXISTE_CLIENTE);
                    }
                    break;
                case FACTURAS:
                    System.out.println("\nEmitir Facturas de un Cliente   ------------");
                    if (! clientes.isEmpty() ) {
                        emitirFacturaCliente(clientes);
                    } else {
                        logger.info(NO_EXISTE_CLIENTE);
                    }
                    break;
                case BORRAR_CLIENTE:
                    System.out.println("\nBorrar Cliente   ------------");
                    if (! clientes.isEmpty() ) {
                        borrarCliente(clientes);
                    } else {
                        logger.info(NO_EXISTE_CLIENTE);
                    }
                    break;
                case CARGAR_CLIENTES_FICH:
                    System.out.println("\nCargar Clientes desde un fichero");
                    try {
						cargarClientes(clientes);
					} catch (IOException e) {
						logger.error(e.getMessage());
					}
                    break;
                case SALVAR_CLIENTES_FICH:
                    System.out.println("\nVolcar Clientes a fichero");
                    if (! clientes.isEmpty() ) {
                        try {
							salvarClientes(clientes);
						} catch (IOException e) {
							logger.error(e.getMessage());
						}
                    } else {
                        logger.info(NO_EXISTE_CLIENTE);
                    }
                    break;
                case CARGAR_COMPRAS_FICH:
                    System.out.println("\nCargar Compras desde un fichero");
                    if (! clientes.isEmpty() ) {
                        try {
							cargarCompra(clientes);
						} catch (IOException e) {
							logger.error(e.getMessage());
						}
                    } else {
                        logger.info(NO_EXISTE_CLIENTE);
                    }
                    break;
                case SALVAR_COMPRAS_FICH:
                    System.out.println("\nVolcar Compras a fichero");
                    if (! clientes.isEmpty() ) {
                        try {
							salvarCompras(clientes);
						} catch (IOException e) {
							logger.error(e.getMessage());
						}
                    } else {
                        logger.info(NO_EXISTE_CLIENTE);
                    }
                    break;
                default:
                    logger.error("Opción no válida.");
                    logger.info("Debe introducir un número entre le 0 y el 9. Gracias.");
            }

        }
    }

    public static int mostrarMenu() {
        int opcion = -1;
        System.out.println("\n");
        System.out.println("\t####  Gestión Facturas Clientes  #########################\n");
        System.out.println("\t1. Mostrar Lista Clientes");
        System.out.println("\t2. Añadir Clientes");
        System.out.println("\t3. Añadir Compras de clientes");
        System.out.println("\t4. Emitir Factura Cliente");
        System.out.println("\t5. Borrar Cliente");
        System.out.println("\t0. Salir\n");
        System.out.println("\t### Tareas Administrativa #################\n");
        System.out.println("\t6. Cargar Clientes desde Fichero");
        System.out.println("\t7. Salvar a fichero los Clientes");
        System.out.println("\t8. Cargar Compras desde Fichero");
        System.out.println("\t9. Salvar a fichero las Compras");
        System.out.println("\n");
        Scanner scan = new Scanner(System.in);
        try {
            System.out.print(">>> Elija una de las opciones > ");
            opcion = scan.nextInt();
        } catch (InputMismatchException e) {
            logger.debug("Entrada no valida. Debes insertar un número:  " + opcion);
            scan.next();
        }
        return opcion;
    }

    public static void mostrarListaClientes(ArrayList<Cliente> clientes) {
        Iterator<Cliente> itrClientes = clientes.iterator();
        System.out.println("Nombre\tDNI\t\tDirección\t\tCP\t\tProvincia");
        while (itrClientes.hasNext()) {
            Cliente cliente = itrClientes.next();
            cliente.mostrarCliente();
        }
    }

    public static Cliente addCliente() {
        Scanner scan = new Scanner(System.in);
        System.out.print(INTRO_NOMBRE);
        String nombre = scan.nextLine();

        System.out.print(">>> Introduzca el DNI del cliente : ");
        String dni = scan.nextLine();

        System.out.print(">>> Introduzca la direccion de facturación : ");
        String dir = scan.nextLine();

        System.out.print(">>> Introduzca el código postal : ");
        String cp = scan.nextLine();

        System.out.print(">>> Introduzca la provincia : ");
        String provincia = scan.nextLine();

        Cliente cliente = new Cliente(nombre, dni, dir, cp, provincia);
        System.out.println("Info: Cliente añadido: " + cliente.getNombre());
        
        return cliente;
    }

    public static void addComprasCliente(ArrayList<Cliente> clientes) {
        Scanner scan = new Scanner(System.in);
        boolean encontrado = false;
        System.out.print(INTRO_NOMBRE);
        String nombre = scan.nextLine();

        Iterator<Cliente> itrClientes = clientes.iterator();
        int i = 0;
        while (itrClientes.hasNext()) {
            Cliente cliente = itrClientes.next();
            if (nombre.equals(cliente.getNombre())) {
                logger.info(CLIENTE_NO_ENCONTRADO);
                encontrado = true;
                break;
            }
            i++;
        }
        if (encontrado) {
            System.out.print(">>> Introduzca nombre del producto comprado por " + clientes.get(i).getNombre() + " : ");
            String producto = scan.nextLine();

            System.out.print(">>> Introduzca las cantidades compradas del " + producto + " : ");
            int unidades = scan.nextInt();

            System.out.print(">>> Introduzca el precio del " + producto + " comprado : ");
            float precio = scan.nextFloat();
            clientes.get(i).addCompra(producto, unidades, precio);
        } else {
            logger.info(CLIENTE_NO_ENCONTRADO);
        }

    }

    public static void borrarCliente(ArrayList<Cliente> clientes) {
        Scanner scan = new Scanner(System.in);
        boolean encontrado = false;
        System.out.print(INTRO_NOMBRE);
        String nombre = scan.nextLine();

        Iterator<Cliente> itrClientes = clientes.iterator();
        while (itrClientes.hasNext()) {
            Cliente cliente = itrClientes.next();
            if (nombre.equals(cliente.getNombre())) {
                encontrado = true;
                System.out.println("Info: El cliente " + nombre + " ha sido borrado");
                itrClientes.remove();
            }
        }
        if (!encontrado) {
            logger.info(CLIENTE_NO_ENCONTRADO);
        }
    }

    public static void emitirFacturaCliente(ArrayList<Cliente> clientes) {
        Scanner scan = new Scanner(System.in);
        boolean encontrado = false;
        System.out.print(INTRO_NOMBRE);
        String nombre = scan.nextLine();
        Iterator<Cliente> itrClientes = clientes.iterator();
        while (itrClientes.hasNext()) {
            Cliente cliente = itrClientes.next();
            if (nombre.equals(cliente.getNombre())) {
                encontrado = true;
                Factura.imprimirFactura(cliente);
                break;
            }
        }
        if (!encontrado) {
            logger.info(CLIENTE_NO_ENCONTRADO);
        }
    }

    public static void cargarClientes(ArrayList<Cliente> clientes) throws IOException {
        BufferedReader dis = null;
        try {
            dis = new BufferedReader(new FileReader(DATA_DIR+"/"+FILE_CLIENTES));
            String linea = dis.readLine();
            while (linea != null) {
                logger.info(linea);
                Cliente cliente = new Cliente();
                cliente.loadClienteFromFile(linea);
                clientes.add(cliente);
                linea = dis.readLine();
            }
        } catch (IOException e) {
            logger.error(e);
            throw e;
        } finally {
            if (dis != null) dis.close();
        }
    }

    public static void cargarCompra(ArrayList<Cliente> clientes) throws IOException {
        BufferedReader dis = null;
        try {
            dis = cargarFichero(DATA_DIR+"/"+FILE_COMPRAS);

            int contador = 0;
            String linea = dis.readLine();
            logger.debug(linea);  
            while (linea != null) {
                logger.info(linea);
                String[] nombre = linea.toString().split("[|]");
                Iterator<Cliente> itrClientes = clientes.iterator();
                while (itrClientes.hasNext()) {
                    Cliente cliente = itrClientes.next();
                    if (nombre[0].equals(cliente.getNombre())) {
                        cliente.loadCompraFromFile(linea);
                        contador++;
                        break;
                    } else {
                        logger.debug("" + nombre + " no encontrado.");
                    }
                }
                linea = dis.readLine();
            }
            logger.info("Se han modificado " + contador + " compras.");
        } catch (IOException e) {
            logger.debug(e.getMessage());
            throw e;
        }
    }

    public static void salvarClientes(ArrayList<Cliente> clientes) throws IOException {
        BufferedWriter dos = null;
        try {
            dos = new BufferedWriter(new FileWriter(DATA_DIR+"/"+FILE_CLIENTES));
            Iterator<Cliente> itrClientes = clientes.iterator();
            while (itrClientes.hasNext()) {
                Cliente cliente = itrClientes.next();
                cliente.saveClienteToFile(dos);
            }
        } catch (IOException e) {
            throw e;
        } finally {
			if (dos != null) dos.close();
        }
    }

    public static void salvarCompras(ArrayList<Cliente> clientes) throws IOException {
        BufferedWriter dos = null;
        try {
            dos = new BufferedWriter(new FileWriter(DATA_DIR+"/"+FILE_COMPRAS));
            Iterator<Cliente> itrClientes = clientes.iterator();
            while (itrClientes.hasNext()) {
                Cliente cliente = itrClientes.next();
                cliente.saveCompraToFile(dos);
            }
        } catch (IOException e) {
            throw e;
        } finally {
            if (dos != null) dos.close();
        }
    }

    public static BufferedReader cargarFichero(String fichero) throws IOException {
        logger.debug("Cargando fichero :: " + fichero);
        try ( BufferedReader dis = new BufferedReader(new FileReader(fichero)) ) {
            return dis;
        } catch (IOException e) {
            logger.debug(e.getMessage());
            throw e;
        }
    }


}
