package es.candela.appfactura;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.net.MalformedURLException;

/**
 * Unit test for simple App.
 */
class ClienteTest {

    private static Logger LOGGER = null;
    private Cliente cliente = null;

    @BeforeAll
    public static void setLogger() throws MalformedURLException {
        System.setProperty("log4j.configurationFile", "log4j2-testConfig.xml");
        LOGGER = LogManager.getLogger();
    }

    @BeforeEach                                         
    public void setUp() throws Exception {
        cliente = new Cliente("Juan","52123432","calla Haya", "28822", "Madrid");
    }
    /**
     * Rigorous Test.
     */
    @Test
    void test_cliente_codigo_postal() {
        LOGGER.info("test cliente codigo Postal Logged!!!");
        assertEquals("28822", cliente.getCodigo_postal() );
    }

    @Test
    void test_cliente_nombre() {
        LOGGER.info("test cliente Nombre Logged !!!");
        assertEquals("Juan", cliente.getNombre() );
    }

    @Test
    void test_add_comprar() {
        cliente.addCompra("Leche", 12, (float)1.5);
        assertEquals(18, cliente.mostrarTotalCompra() );
    }

    @Test
    void test_add_dos_comprar() {
        cliente.addCompra("Leche", 12, (float)1.5);
        cliente.addCompra("Pan", 2, (float)1.5);
        assertEquals(21, cliente.mostrarTotalCompra() );
    }

}